import React, { Component } from "react";
import {
    Container,
    Content,
    Button,
    List,
    ListItem,
    Text,
    Thumbnail,
    Left,
    Body,
    Right
} from "native-base";
import {AsyncStorage} from 'react-native';
import styles from "./styles";
import {withNavigation} from "react-navigation";
import DetailMedicamentSante from "../DetailMedicamentSante/DetailMedicamentSante";


const supriya = require("../../../assets/contacts/supriya.png");

const datas = [

    {
        img: supriya,
        text: "Supriya",

    },

];

class tabOne extends Component {
    constructor(){
        super();
        this.state={
            produits:[]
        }
    }
    componentDidMount() {
        this.getAll();
    }

    /////async= asynchrone
    //// on ne peut execute  getall avant avoir le token dans AsyncStorage
    //await estaneniiii
    async  getAll() {
        const token=await AsyncStorage.getItem("token");
        console.log("token ",token);

        const headers={
            "Authorization":"Bearer "+token
        }
        fetch('http://192.168.1.54:8080/produits/all', {method: 'GET', headers: headers})
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({produits: data});
            });
    }

    goDetail=(id)=>{
        console.log("id ", id);
        const ch=""+id+"";
        AsyncStorage.setItem("id",ch);
        this.props.navigation.navigate('DetailMedicamentSante');
    }

    render() {
        return (
            <Container style={styles.container}>

                <Content>
                    <List
                        dataArray={this.state.produits}
                        renderRow={data =>
                            <ListItem thumbnail>
                                <Left>
                                    <Thumbnail square source={{uri:`http://192.168.1.54:8080/files/${data.photo}`}} />
                                </Left>
                                <Body>
                                <Text>
                                    {data.refernece}
                                </Text>
                                <Text numberOfLines={1} note>
                                    {data.nomProduit}
                                </Text>
                                <Text numberOfLines={1} note>
                                    {data.datedexperation}
                                </Text>
                                <Text numberOfLines={1} note>
                                    {data.datefabrication}
                                </Text>
                                <Text numberOfLines={1} note>
                                    {data.prix}
                                </Text>
                                </Body>
                                <Right>
                                    <Button transparent>
                                        <Text onPress={() => {this.goDetail(data.id)}}>Detail</Text>
                                    </Button>
                                </Right>
                            </ListItem>}
                    />
                </Content>
            </Container>
        );
    }
}

export default withNavigation(tabOne);
