import React, { Component } from "react";
import {
    Container,
    Content,
    Button,
    List,
    ListItem,
    Text,
    Thumbnail,
    Left,
    Body,
    Right
} from "native-base";
import styles from "./styles";

const sankhadeep = require("../../../assets/contacts/sankhadeep.png");

const datas = [
    {
        img: sankhadeep,
        text: "Sankhadeep",

    },

];
import {withNavigation} from "react-navigation";

class tabTwo extends Component {
    componentDidMount(){
        console.log("tabTwo s")
    }
    render() {
        return (
            <Container style={styles.container}>

                <Content>
                    <List
                        dataArray={datas}
                        renderRow={data =>
                            <ListItem thumbnail>
                                <Left>
                                    <Thumbnail square source={data.img} />
                                </Left>
                                <Body>
                                <Text>
                                    {data.text}
                                </Text>
                                <Text numberOfLines={1} note>
                                    {data.note}
                                </Text>
                                </Body>
                                <Right>
                                    <Button transparent>
                                        <Text onPress={() => {
                                            this.props.navigation.navigate('DetailMedicamentBeaute') }}>Détails</Text>
                                    </Button>
                                </Right>
                            </ListItem>}
                    />
                </Content>
            </Container>
        );
    }
}

export default withNavigation(tabTwo);
