import React, { Component } from "react";
import { Image, Dimensions } from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Icon,
    Card,
    CardItem,
    Text,
    Thumbnail,
    Left,
    Right,
    Body
} from "native-base";
import {AsyncStorage} from'react-native';
import styles from "./styles";


const deviceWidth = Dimensions.get("window").width;
const medicaments = require("../../../assets/medicaments.jpg");
const cardImage = require("../../../assets/drawer-cover.png");
const pratik = require("../../../assets/contacts/1137037.jpg");

class NHCardShowcase extends Component {

    constructor() {
        super();
        this.state = {
            pharmacie: {
                nomPh: '',
                matricule: '',
                ville: '',
                adresse: '',
                localisation: '',
                email: '',
                password: '',
                username: '',
                tel: '',
            }
        };


    }

   async componentDidMount() {
       this.getone();


    }

   async getone() {
       const token=await AsyncStorage.getItem("token");
       const id=await AsyncStorage.getItem("idPharmacie");
       console.log("token ",token);
       console.log("id ",id);

       const headers={
           "Authorization":"Bearer "+token
       }
        fetch('http://192.168.1.54:8080/pharmacie/one/'+id, {method: 'GET', headers: headers})
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({pharmacie: data});
            });
    }

    render() {
        return (
            <Container style={styles.container}>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back" />
                        </Button>
                    </Left>
                    <Body>
                    <Title>Detail pharmacie</Title>
                    </Body>
                    <Right />
                </Header>

                <Content padder>
                    <Card style={styles.mb}>
                        <CardItem bordered>
                            <Left>
                                <Thumbnail source={pratik} />
                                <Body>
                                <Text style={{color: "rgb(0, 154, 0)" ,
                                    fontSize: 25,
                                    }}>{this.state.pharmacie.nomPh}</Text>
                                </Body>
                            </Left>
                        </CardItem>

                        <CardItem>
                            <Body>
                            <Image
                                style={{
                                    alignSelf: "center",
                                    height: 150,
                                    resizeMode: "cover",
                                    width: deviceWidth / 1.18,
                                    marginVertical: 5
                                }}
                                source={medicaments}
                            />
                            <Text>
                                <Icon active name="home" style={{color: "#E14343",
                                fontSize:20 }}/>
                                Ville:
                                {this.state.pharmacie.ville}
                            </Text>

                            <Text>
                                <Icon name="paper-plane" style={{ color: "#E14343",
                                    fontSize:20 }} />
                                Adresse:
                                {this.state.pharmacie.adresse}
                            </Text>
                            <Text>
                                <Icon name="mail" style={{ color: "#E14343",
                                    fontSize:20 }} />
                                Email:
                                {this.state.pharmacie.email}
                            </Text>
                            </Body>
                        </CardItem>
                        <CardItem style={{ paddingVertical: 0 ,
                            marginLeft:80,
                            width:700,

                           }}>
                            <Button rounded light style={styles.mb15}>
                                <Icon  name="logo-whatsapp" style={{color: "rgb(0, 154, 0)"}}/>
                            </Button>
                            <Button rounded light style={styles.mb15}>
                                <Icon  name="navigate" style={{color: "rgb(0, 154, 0)"}}/>
                            </Button>
                            <Button rounded light style={styles.mb15} onPress={() => {
                                this.props.navigation.navigate("listMedicament");
                            }}>
                                <Icon  name="medkit" style={{color: "rgb(0, 154, 0)"}} />
                            </Button>
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        );
    }
}

export default NHCardShowcase;
