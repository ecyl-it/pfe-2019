import React, { Component } from "react";
import { ImageBackground, View, StatusBar } from "react-native";
import { Container, Button,Text } from "native-base";

import styles from "./styles";
import Login from "../Login/Login";

const launchscreenBg = require("../../../assets/depositphotos_192911234-stock-video-green-screen-green-background-green.jpg");
const launchscreenLogo = require("../../../assets/logop.png");

class Home extends Component {
  render() {
    return (
      <Container>
        <StatusBar barStyle="light-content" />
        <ImageBackground source={launchscreenBg} style={styles.imageContainer}>
          <View style={styles.logoContainer}>
            <ImageBackground source={launchscreenLogo} style={styles.logo} />
          </View>
          <View
            style={{
              alignItems: "center",
              marginBottom: 50,
              backgroundColor: "transparent"
            }}
          >


          </View>
          <View style={{ marginBottom: 80 }}>
            <Button
              style={{ backgroundColor: "#6FAF98", alignSelf: "center" }}
              onPress={() => this.props.navigation.navigate('Login')}
            >
              <Text>Lets Go!</Text>
            </Button>
          </View>
        </ImageBackground>
      </Container>
    );
  }
}

export default Home;
