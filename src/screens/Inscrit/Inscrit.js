import React, {Component} from "react";
import { View, StatusBar } from "react-native";
import {Container, Text, Button, Form, Input, Icon, Content, Item ,Radio,Left,ListItem,Right,Header,Body,Title,Footer,FooterTab} from "native-base";
import DatePicker from "react-native-datepicker";

import Style from "./Style";
import axios from "axios";

const launchscreenBg = require("../../../assets/launchscreen-bg.png");
const launchscreenLogo = require("../../../assets/logo-kitchen-sink.png");

class Inscrit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dateN: "",
            adresse: "",
            email: "",
            password: "",
            username: "",
            tel: "",
            genre: "",
            ville: "",
            firstName: "",
            lastName: "",
            radio1: false,
            radio2: false
        };
    }
    toggleRadio1() {
        this.setState({
            radio1: true,
            radio2: false,

        });
        this.setState({genre:"Homme"})
    }
    toggleRadio2() {
        this.setState({
            radio1: false,
            radio2: true,
        });
        this.setState({genre:"Femme"})
    }

    register =()=>{
        console.log("state ", this.state)
        const headers={
            "Content-Type":"application/json "
        }

        axios.post("http://192.168.1.54:8080/client/add", {
            dateN: this.state.dateN,
            adresse: this.state.adresse,
            email: this.state.email,
            password: this.state.password,
            username: this.state.username,
            genre: this.state.genre,
            ville: this.state.ville,
            username: this.state.username,
            firstName: this.state.firstName,
            tel: this.state.tel,
            lastName: this.state.lastName,
        },{headers:headers}).then(res => {
            console.log("data ",res.data);

            this.props.navigation.navigate('Login')
        });
    }


    render() {
        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back" />
                            <Text>Back</Text>
                        </Button>
                    </Left>
                    <Body>
                    <Title>inscription</Title>
                    </Body>

                </Header>

                <View style={Style.container}>

                    <Content padder>
                        <Form style={Style.form}>
                            <Item>
                                <Icon active name="person" style={{color: "#00aced"}}/>
                                <Input placeholder="Nom"
                                       value={this.state.firstName}
                                       onChangeText={(text) => this.setState({firstName: text})}
                                />
                            </Item>
                            <Item>
                                <Icon active name="person" style={{color: "#00aced"}}/>
                                <Input placeholder="Prenom"
                                       value={this.state.lastName}
                                       onChangeText={(text) => this.setState({lastName: text})}/>
                            </Item>
                            <Item>
                                <Icon active name="mail" style={{color: "#00aced"}}/>
                                <Input placeholder="Email"
                                       value={this.state.email}
                                       onChangeText={(text) => this.setState({email: text})}/>
                            </Item>
                            <Item>
                                <Icon active name="mail" style={{color: "#00aced"}}/>
                                <Input placeholder="Username"
                                       value={this.state.username}
                                       onChangeText={(text) => this.setState({username: text})}/>
                            </Item>
                            <Item>
                                <Icon active name="eye" style={{color: "#00aced"}}/>
                                <Input placeholder="Password"
                                       value={this.state.password}
                                       onChangeText={(text) => this.setState({password: text})}
                                       secureTextEntry/>
                            </Item>
                            <Item>
                                <Icon active name="home" style={{color: "#00aced"}}/>
                                <Input placeholder="Adresse"
                                       value={this.state.adresse}
                                       onChangeText={(text) => this.setState({adresse: text})}/>
                            </Item>
                            <Item>
                               <Icon active name="logo-whatsapp" style={{color: "#00aced"}}/>
                                <Input placeholder="Tel"  value={this.state.tel}
                                       onChangeText={(text) => this.setState({tel: text})}/>
                            </Item>

                            <ListItem style={Style.radio}
                                selected={this.state.radio1}
                                onPress={() => this.toggleRadio1()}
                            >
                                <Left>
                                    <Text>Homme</Text>
                                </Left>
                                <Right>
                                    <Radio
                                        color={"#f0ad4e"}
                                        selectedColor={"#5cb85c"}
                                        selected={this.state.radio1}
                                        onPress={() => this.toggleRadio1()}
                                    />
                                </Right>
                            </ListItem>
                            <ListItem style={Style.radio}
                                selected={this.state.radio2}
                                onPress={() => this.toggleRadio2()}
                            >
                                <Left>
                                    <Text>Femme</Text>
                                </Left>
                                <Right>
                                    <Radio
                                        color={"#f0ad4e"}
                                        selectedColor={"#5cb85c"}
                                        selected={this.state.radio2}
                                        onPress={() => this.toggleRadio2()}
                                    />
                                </Right>
                            </ListItem>
                            <Item>
                                <Icon active name="home" style={{color: "#00aced"}}/>
                                <Input placeholder="Ville"  value={this.state.ville}
                                       onChangeText={(text) => this.setState({ville: text})}/>
                            </Item>
                            <DatePicker
                                style={{width: 200}}
                                date={this.state.dateN}
                                mode="date"
                                placeholder="select date"
                                format="YYYY-MM-DD"
                                minDate="1970-01-01"
                                maxDate="2020-12-30"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    dateIcon: {
                                        position: "absolute",
                                        left: 0,
                                     width:20,
                                       top:20,
                                        marginLeft: 50
                                    },
                                    dateInput: {
                                        marginLeft: 80,
                                        top:20,
                                    }
                                    // ... You can check the source to find the other keys.
                                }}
                                onDateChange={(date) => {
                                    this.setState({dateN: date});
                                }}
                            />
                            <View style={Style.View}>
                                <Button block success style={Style.test}
                                        onPress={() => {
                                            this.register();
                                        }}>
                                    <Text>Register</Text>
                                </Button>

                            </View>

                        </Form>
                    </Content>
                </View>

                <Footer>
                    <FooterTab>


                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

export default Inscrit;
