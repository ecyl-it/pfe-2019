import React, { Component } from "react";
import {
    Container,
    Header,
    Content,
    Button,
    Icon,
    List,
    ListItem,
    Text,
    Thumbnail,
    Left,
    Body,
    Right,
    Item,
    Input
} from "native-base";
import {AsyncStorage} from 'react-native';
import styles from "./styles";


const pratik = require("../../../assets/contacts/1137037.jpg");


class NHListThumbnail extends Component {
    constructor(){
        super();
        this.state={
            pharmacie:[]
        }
    }
    componentDidMount() {
        this.getAll();
    }

    /////async= asynchrone
    //// on ne peut execute  getall avant avoir le token dans AsyncStorage
    //await estaneniiii
  async  getAll() {
        const token=await AsyncStorage.getItem("token");
        console.log("token ",token);

        const headers={
            "Authorization":"Bearer "+token
        }
        fetch('http://192.168.1.54:8080/pharmacie/all', {method: 'GET', headers: headers})
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({pharmacie: data});
            });
    }

  goDetail=(id)=>{
        console.log("id ", id);
        const ch=""+id+"";
      AsyncStorage.setItem("idPharmacie",ch);
        this.props.navigation.navigate('DetailPharmacy');
    }

    render() {
        return (
            <Container style={styles.container}>
                <Header searchBar rounded>
                    <Item>
                        <Icon active name="search" />
                        <Input placeholder="Search" />
                        <Icon active name="people" />
                    </Item>
                    <Button transparent>
                        <Text>Search</Text>
                    </Button>
                </Header>

                <Content>
                    <List
                        dataArray={this.state.pharmacie}
                        renderRow={data =>
                            <ListItem thumbnail>
                                <Left>
                                    <Thumbnail square source={pratik} />
                                </Left>
                                <Body>
                                <Text>
                                    {data.nomPh}
                                </Text>
                                <Text>
                                    {data.ville}
                                </Text>
                                </Body>
                                <Right>
                                    <Button transparent>
                                        <Text onPress={() => {this.goDetail(data.id)}}>Detail</Text>
                                    </Button>
                                </Right>
                            </ListItem>}
                    />
                </Content>
            </Container>
        );
    }
}

export default NHListThumbnail;
