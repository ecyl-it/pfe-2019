import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  Left,
  Right,
  Body,
    Form,
    CardItem,
    Item,
    Label,
    Input,
    Text,
    DatePicker
} from "native-base";
import { Grid, Row } from "react-native-easy-grid";
import styles from "../DetailPharmacy/styles";


class RowNB extends Component {
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>payement</Title>
          </Body>
          <Right />
        </Header>


              <CardItem>
                  <Button rounded light style={styles.mb15}>
                      <Icon  name="logo-whatsapp" style={{color: "rgb(0, 154, 0)"}}/>
                  </Button>
                  <Button rounded light style={styles.mb15}>
                      <Icon  name="navigate" style={{color: "rgb(0, 154, 0)"}}/>
                  </Button>
                  <Button rounded light style={styles.mb15} onPress={() => {
                      this.props.navigation.navigate("listMedicament");
                  }}>
                      <Icon  name="medkit" style={{color: "rgb(0, 154, 0)"}} />
                  </Button>
              </CardItem>




              <Form>
                  <Item inlineLabel>
                      <Label>Numero de carte</Label>
                      <Input />
                  </Item>
                  <DatePicker
                      defaultDate={new Date(2018, 4, 4)}
                      minimumDate={new Date(2018, 1, 1)}
                      maximumDate={new Date(2018, 12, 31)}
                      locale={"en"}
                      timeZoneOffsetInMinutes={undefined}
                      modalTransparent={false}
                      animationType={"fade"}
                      androidMode={"default"}
                      placeHolderText="Select date"
                      textStyle={{ color: "green" }}
                      placeHolderTextStyle={{ color: "#d3d3d3" }}
                      onDateChange={this.setDate}
                  />
              </Form>
              <Item inlineLabel>
                  <Label>Code de securite</Label>
                  <Input />
              </Item>
              <Item inlineLabel>
                  <Label>Adresse</Label>
                  <Input />
              </Item>
              <Item inlineLabel>
                  <Label>Code postal</Label>
                  <Input />
              </Item>
              <Button block style={{ margin: 15, marginTop: 50 }}>
                  <Text>Valider</Text>
              </Button>

          <Button block style={{ margin: 15, marginTop: 50 }}>
              <Text>Abondonner</Text>
          </Button>

      </Container>
    );
  }
}

export default RowNB;
