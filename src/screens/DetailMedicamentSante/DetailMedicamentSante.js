import React, { Component } from "react";
import { Image, Dimensions } from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Icon,
    Card,
    CardItem,
    Text,
    Thumbnail,
    Left,
    Right,
    Body
} from "native-base";
import styles from "./styles";
import {AsyncStorage} from'react-native';
const deviceWidth = Dimensions.get("window").width;
const logo = require("../../../assets/logo.png");
const cardImage = require("../../../assets/drawer-cover.png");

class NHCardShowcase extends Component {
    constructor() {
        super();
        this.state = {
            produits: {
                refernece: '',
                nomProduit: '',
                datedexperation: '',
                datefabrication: '',
                prix: '',
                photo:'',
            }
        };


    }

    async componentDidMount() {
        this.getone();


    }

    async getone() {
        const token=await AsyncStorage.getItem("token");
        const id=await AsyncStorage.getItem("id");
        console.log("token ",token);
        console.log("id ",id);

        const headers={
            "Authorization":"Bearer "+token
        }
        fetch('http://192.168.1.54:8080/produits/one/'+id, {method: 'GET', headers: headers})
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({produits: data});
            });
    }
    render() {
        return (
            <Container style={styles.container}>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back" />
                        </Button>
                    </Left>
                    <Body>
                    <Title>Card Showcase</Title>
                    </Body>
                    <Right />
                </Header>

                <Content padder>
                    <Card style={styles.mb}>
                        <CardItem bordered>
                            <Left>
                                <Thumbnail source={logo} />
                                <Body>
                                <Text>Detail Medicaments Sante</Text>

                                </Body>
                            </Left>
                        </CardItem>

                        <CardItem>
                            <Body>
                            <Image
                                style={{
                                    alignSelf: "center",
                                    height: 150,
                                    resizeMode: "cover",
                                    width: deviceWidth / 1.18,
                                    marginVertical: 5
                                }}
                                source={cardImage}
                            />
                            <Text>
                                {this.state.pharmacie.refernece}
                            </Text>
                            <Text>
                                {this.state.pharmacie.nomProduit}
                            </Text>
                            <Text>
                                {this.state.pharmacie.datedexperation}
                            </Text>
                            <Text>
                                {this.state.pharmacie.datefabrication}
                            </Text>
                            <Text>
                                {this.state.pharmacie.prix}
                            </Text>

                            </Body>
                        </CardItem>
                        <CardItem style={{ paddingVertical: 0 }}>
                            <Left>
                                <Button transparent>
                                    <Icon name="logo-github" />
                                    <Text>4,923 stars</Text>
                                </Button>
                            </Left>
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        );
    }
}

export default NHCardShowcase;
