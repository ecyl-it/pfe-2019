import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Body,
  Left,
  Right,
  Item,
    Label,
  Form,
    Textarea,
    DatePicker,
    Text,
    Input
} from "native-base";
import styles from "./styles";

class Rounded extends Component {
  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Reclamations</Title>
          </Body>
          <Right />
        </Header>

        <Content padder>
          <Form>
              <Item>
                  <Input placeholder="nom pharmacie" />
              </Item>
              <DatePicker
                  defaultDate={new Date(2018, 4, 4)}
                  minimumDate={new Date(2018, 1, 1)}
                  maximumDate={new Date(2018, 12, 31)}
                  locale={"en"}
                  timeZoneOffsetInMinutes={undefined}
                  modalTransparent={false}
                  animationType={"fade"}
                  androidMode={"default"}
                  placeHolderText="Select date"
                  textStyle={{ color: "green" }}
                  placeHolderTextStyle={{ color: "#d3d3d3" }}
                  onDateChange={this.setDate}
              />
              <Textarea rowSpan={5} bordered placeholder="object" />
              <Button block style={{ margin: 15, marginTop: 50 }}>
                  <Text>Envoyer</Text>
              </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

export default Rounded;
