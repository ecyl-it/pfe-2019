import React, {Component} from "react";
import {Platform, StyleSheet, Text, View, TextInput, Button, Alert, AsyncStorage} from "react-native";
import axios from "axios";
import Inscrit from "../Inscrit/Inscrit";
import ListPharmacy from "../ListPharmacy/Listpharmacy";

const instructions = Platform.select({
    ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
    android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu",
});

type Props = {};
export default class Login extends Component<Props> {
    constructor() {
        super();
        this.state = {
            username: "",
            password: ""
        };
    }

    componentDidMount() {
        console.log("login ");
    }

    login = () => {
        console.log("state ", this.state);

        axios.post("http://192.168.1.54:8080/users/login", {username:this.state.username,password:this.state.password})
            .then(res => {


                console.log("data ", res);


                if (res.data['data'] === null) {
                   Alert.alert("votre mot de passe ou username incorrecte");
                }
                else if (res.data['data']['body']['user']['role'] === "CLIENT") {
                    AsyncStorage.setItem("token", res.data['data']['body']['access_token']);
                    AsyncStorage.setItem("idClient", res.data['data']['body']['user']['id']);
                    console.log("token ",res.data['data']['body']['access_token'] )
                    this.props.navigation.navigate('ListPharmacy');
                }
                else {
                    Alert.alert("votre mot de passe ou username incorrecte");
                }
            }).catch(err => {

            console.log("errr" + err);
        });

    };

    render() {
        return (
            <View style={styles.container}>

                <TextInput
                    style={styles.text}
                    placeholder="Mail"
                    value={this.state.username}
                    onChangeText={(text) => this.setState({username: text})}
                />
                <TextInput
                    style={styles.text}
                    placeholder="Password"
                    value={this.state.password}
                    onChangeText={(text) => this.setState({password: text})}
                    secureTextEntry
                />
                <View style={styles.View}>
                    <Button
                        title="connect"
                        style={styles.button}
                        onPress={() => {
                            this.login();
                        }}
                    />

                </View>
                <Text style={styles.Label} onPress={() => {
                    this.props.navigation.navigate("Inscrit");
                }}> create account? </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#7e6f76",

    },
    text: {
        borderColor: "red",
        backgroundColor: "#fff",
        borderRadius: 2,
        height: 40,
        marginTop: 10,
        width: 300,
        opacity: 0.2
    },

    button: {
        backgroundColor: "#fff",
        borderRadius: 2,
        height: 60,
        width: 300,
        marginTop: 10,
        marginBottom: 10,
        color: "#ee32df"

    },
    Label: {
        fontWeight: "bold",
        fontSize: 15,
        marginTop: 10,
        marginLeft: 100,
        width: 200,
        height: 80,
        justifyContent: "center",
        alignItems: "center",
        color: "#fff",
        textDecorationLine: "underline"
    },
    View: {
        marginVertical: 10,
        marginTop: 10,
        marginBottom: 10,
        height: 60,
        width: 300,

    }
});
