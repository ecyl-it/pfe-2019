import React, { Component } from "react";
import {
    Container,
    Header,
    Title,
    Button,
    Icon,
    Tabs,
    Tab,
    Right,
    Left,
    Body,
    ScrollableTab
} from "native-base";

import TabOne from "./tabOne";
import TabTwo from "./tabTwo";


class ListMedicament extends Component {

    render() {
        return (
            <Container>
                <Header hasTabs>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back" />
                        </Button>
                    </Left>
                    <Body>
                    <Title> Liste des Medicament</Title>
                    </Body>
                    <Right />
                </Header>
                <Tabs renderTabBar={() => <ScrollableTab />}>
                    <Tab heading="Santé">
                        <TabOne />
                    </Tab>
                    <Tab heading="Beauté">
                        <TabTwo />
                    </Tab>
                </Tabs>
            </Container>
        );
    }
}

export default ListMedicament;
